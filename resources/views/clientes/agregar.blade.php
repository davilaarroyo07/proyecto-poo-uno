@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">CREAR CLIENTES</div>
                <div class="d-grid gap-2 col-6 mx-auto">
                <a class="btn btn-primary" type="button" href="{{route('listar.clientes')}}">Ver lista de clientes</a>
               </div>
                <div class="card-body">
                    <form role="form" method="POST" action="{{route('guardar.clientes')}}">
                        {{ csrf_field() }}
                        {{method_field('post')}}
  <div class="mb-3">
    <label for="nombre" class="form-label">Nombres</label>
    <input type="text" class="form-control" name="nombre">
  </div>
  <div class="mb-3">
    <label for="apellidos" class="form-label">Apellidos</label>
    <input type="text" class="form-control" name="apellidos">
  </div>
  <div class="mb-3">
    <label for="cedula" class="form-label">Cédula</label>
    <input type="number" class="form-control" name="cedula" >
  </div>
  <div class="mb-3">
    <label for="direccion" class="form-label">Dirección</label>
    <input type="text" class="form-control" name="direccion">
  </div>
  <div class="mb-3">
    <label for="telefono" class="form-label">Teléfono</label>
    <input type="text" class="form-control" name="telefono">
  </div>
  <div class="mb-3">
    <label for="fecha_nacimiento" class="form-label">Fecha de nacimiento</label>
    <input type="text" class="form-control" name="fecha_nacimiento">
  </div>
  <div class="mb-3">
    <label for="email" class="form-label">Correo electrónico</label>
    <input type="email" class="form-control" name="email">
  </div>
  <button type="submit" class="btn btn-primary">Guardar datos</button>
</form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
