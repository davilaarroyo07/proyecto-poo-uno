@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">LISTA DE CLIENTES</div>
                <div class="d-grid gap-2 col-6 mx-auto">
                <a class="btn btn-primary" type="button" href="{{route('agregar.clientes')}}">Crear cliente</a>
               </div>
                <div class="card-body">
                    <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Nombres</th>
      <th scope="col">Apellidos</th>
      <th scope="col">Cédula</th>
      <th scope="col">Dirección</th>
      <th scope="col">Teléfono</th>
      <th scope="col">Fecha de nacimiento</th>
      <th scope="col">Correo electrónico</th>
    </tr>
  </thead>
  <tbody>
      @foreach ( $clientes as $atributo )
    <tr>
       <th scope="row">{{$atributo->id}}</th>
       <td>{{$atributo->nombre}}</td>
       <td>{{$atributo->apellidos}}</td>
       <td>{{$atributo->cedula}}</td>
       <td>{{$atributo->direccion}}</td>
       <td>{{$atributo->telefono}}</td>
       <td>{{$atributo->fecha_nacimiento}}</td>
       <td>{{$atributo->email}}</td>
    </tr>
        @endforeach
  </tbody>
</table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

